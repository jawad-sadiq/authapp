import { StyleSheet } from "react-native";
export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
  },
  loginTitleContainer: {
    width: "88%",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-start",
  },
  loginTitle: {
    fontWeight: "bold",
    fontSize: 26,
  },
  fieldsContainer: {
    flexDirection: "column",
    alignItems: "center",
    marginTop: 10,
    width: "88%",
  },
  field: {
    flexDirection: "row",
    alignItems: "center",
    borderColor: "black",
    borderBottomWidth: 1,
    marginTop: 15,
    marginHorizontal: 20,
  },
  iconContainer: {
    width: "10%",
    height: "100%",
    textAlignVertical: "center",
  },
  icon: {
    paddingTop: 10,
    paddingBottom: 10,
  },
  input: {
    paddingTop: 10,
    paddingBottom: 10,
  },
  forgotContainer: {
    alignItems: "flex-end",
    width: "20%",
  },
  forgotLabel: {
    color: "red",
  },
  loginButtonContainer: {
    justifyContent: "center",
    marginTop: 20,
    alignSelf: "flex-end",
  },
  loginButton: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: "80%",
    height: 50,
    borderRadius: 50,
  },
  footer: {
    position: "absolute",
    bottom: 0,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
  },
});
