import { StyleSheet } from "react-native";
export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
  },
  splash: {
    flex: 1,
    resizeMode: "cover",
  },
});
